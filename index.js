var fs = require('fs');

var movies = JSON.parse(fs.readFileSync('movies.json'))
var IMDb = require('imdb-scraper')

for (var i = 0; i < movies.length; ++i) {
	IMDb.title.first(movies[i].title).then(function(movie) {
		console.log(movie.name + '(' + movie.year + ') ' +
			movie.genre + 
			' http://imdb.com/title/' + movie.id +
			' rating: ' +movie.rating.value)
	});
}